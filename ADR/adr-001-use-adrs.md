# Architecture Decision Record: Use ADRs

## Context

FightingEurope has several goals that make the practice and discipline of architecture decisions very important:

- recognize and test technologies, methodologies and design patterns
- record information of recognized technologies, methodologies and design patterns
- mantain clean and simple design documentation

## Decision

Every architecture-level decision for FightingEurope will be documented with an [Architecture Decision Record](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions). These are a well structured, relatively lightweight way to capture architectural proposals. In ADRs will be also shortly described comparsion with other alternative solutions. 

The workflow will be:

1. After identification of a problem which project must face on, will be added record to adr-todo-list
2. After recognition of possible solutions will be prepared ADR with description of possible solutions and decision which solution and why was choosen wtih "accepted" state.
3. Accepted ADRs should be mantained in master branch
4. If a decision is revisited and a different conclusion is reached, a new ADR should be created documenting the context and rationale for the change. The new ADR should reference the old one, and once the new one is accepted, the old one should (in it's "status" section) be updated to point to the new one. The old ADR should not be removed or otherwise modified except for the annotation pointing to the new ADR.

## Status

Accepted

## Consequences

1. Developers must write an ADR for any architectural decision.
2. The master branch of repository will reflect the high-level consensus of the steering group.
3. Project will have a useful persistent record of why it is the way it is.